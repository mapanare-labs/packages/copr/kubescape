%define debug_package %{nil}

Name:           kubescape
Version:        3.0.21
Release:        1%{?dist}
Summary:        Kubescape is a K8s open-source tool providing a multi-cloud K8s single pane of glass, including risk analysis, security compliance, RBAC visualizer and image vulnerabilities scanning.
Group:          Applications/System
License:        ASL 2.0
URL:            https://www.gruntwork.io/
Source0:        https://github.com/%{name}/%{name}/archive/v%{version}.tar.gz
Source1:        https://github.com/%{name}/%{name}/releases/download/v%{version}/%{name}-ubuntu-latest

%description
Kubescape is a K8s open-source tool providing 
a multi-cloud K8s single pane of glass, including 
risk analysis, security compliance, RBAC visualizer 
and image vulnerabilities scanning.

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 3.0.4

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 3.0.1

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.9.0

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.3.8

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 2.3.6

* Thu May 18 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 2.3.3

* Mon May 08 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 2.3.1

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 2.2.6

* Sat Feb 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 2.2.1

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 2.1.3

* Wed Dec 14 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 2.0.178

* Fri Nov 25 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 2.0.176

* Tue Nov 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 2.0.175

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 2.0.174

* Sat Sep 24 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 2.0.171

* Thu Sep 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
